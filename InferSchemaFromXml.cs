using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace WebServiceTestingToolUtilities
{
    public class InferSchemaFromXml
    {
        public InferSchemaFromXml()
        {
        }

        public byte[] XmlToXsdString(byte[] xml)
        {
            string schemaString ="";
            XmlSchemaSet schemaSet = XmlToXsdXmlSchemaSet(xml);
            foreach (XmlSchema s in schemaSet.Schemas())
            {
                using (var stringWriter = new StringWriter())
                {
                    using (var writer = XmlWriter.Create(stringWriter))
                    {
                        s.Write(writer);
                    }
                    schemaString = string.Concat(schemaString, stringWriter.ToString());
                }
            }
            return Encoding.UTF8.GetBytes(schemaString);
        }

        public string XmlToXsdString(string xml)
        {
            string schemaString = "";
            XmlSchemaSet schemaSet = XmlToXsdXmlSchemaSet(xml);
            foreach (XmlSchema s in schemaSet.Schemas())
            {
                using (var stringWriter = new StringWriter())
                {
                    using (var writer = XmlWriter.Create(stringWriter))
                    {
                        s.Write(writer);
                    }
                    schemaString = string.Concat(schemaString, stringWriter.ToString());
                }
            }
            return schemaString;
        }

        public XmlSchemaSet XmlToXsdXmlSchemaSet(byte[] xml)
        {
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            XmlSchemaInference schema = new XmlSchemaInference();
            XmlReader reader = XmlReader.Create(new StringReader(Encoding.UTF8.GetString(xml)));
            return schema.InferSchema(reader);
        }

        public XmlSchemaSet XmlToXsdXmlSchemaSet(string xml)
        {
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            XmlSchemaInference schema = new XmlSchemaInference();
            XmlReader reader = XmlReader.Create(new StringReader(xml));
            return schema.InferSchema(reader);
        }
    }
}
